/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { format, transports } from "winston";
import helmet from "helmet";
import { LoggingInterceptor, NestjsWinstonLoggerService } from 'nestjs-winston-logger';

import { AppModule } from './app/app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.KAFKA,
      options: {
        client: {
          clientId: 'auth',
          brokers: ['localhost:9092'],
        },
        consumer: {
          groupId: 'auth-consumer',
        },
      },
    }
  );
  // app.use(helmet());
  const globalLogger = new NestjsWinstonLoggerService({
    format: format.combine(
      format.timestamp({ format: "isoDateTime" }),
      format.json(),
      format.colorize({ all: true }),
    ),
    transports: [
      new transports.File({ filename: "logs/microservices/auth/error.log", level: "error" }),
      new transports.File({ filename: "logs/microservices/auth/combined.log" }),
      new transports.Console(),
    ],
  });
  app.useLogger(globalLogger);

  // // append id to identify request
  // app.use(appendIdToRequest);
  // app.use(appendRequestIdToLogger(globalLogger));

  // app.use(morganRequestLogger(globalLogger));
  // app.use(morganResponseLogger(globalLogger));

  app.useGlobalInterceptors(new LoggingInterceptor(globalLogger));
  await app.listen();
}

bootstrap();
