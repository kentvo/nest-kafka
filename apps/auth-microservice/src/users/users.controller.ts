import { UserDto } from '@dexview-application/shared/dtos';
import { Controller, ParseIntPipe, ValidationPipe } from '@nestjs/common';
import { EventPattern, MessagePattern, Payload } from '@nestjs/microservices';
import { InjectLogger, NestjsWinstonLoggerService } from 'nestjs-winston-logger';
import { UserService } from './users.service';

@Controller()
export class UsersController {
  constructor(private readonly userService: UserService,
    @InjectLogger(UsersController.name) private logger: NestjsWinstonLoggerService) {}

  @EventPattern('create_user')
  handleUserCreate(@Payload(ValidationPipe) data: UserDto) {
    this.logger.log("handleUserCreate");
    this.userService.createUser(data);
  }

  @EventPattern('get_user')
  async handleGetUser(@Payload(ValidationPipe) id: number): Promise<UserDto> {
    return await this.userService.getUser(id);
  }
}