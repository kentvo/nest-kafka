import { Injectable, NotFoundException } from '@nestjs/common';
import { UserEntity } from '@dexview-application/shared/entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserDto } from '@dexview-application/shared/dtos';
import { plainToInstance } from 'class-transformer';
import { LoggerService } from '@nestjs/common';
import { LogLevel } from '@nestjs/common/services';
@Injectable()
export class UserRepository {

  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>
  ) {}
  
  private readonly users: UserEntity[] = [];

  async save(userDto: UserDto): Promise<UserDto> {
    const savedUser = await this.userRepository.save(userDto);
    console.log("[AUTH_MICROSERVICE] call save to db successfully ", savedUser);
    return plainToInstance(UserDto, savedUser, {
        excludeExtraneousValues: true
    });
  }

  async findOne(id: number): Promise<UserDto> {
    const userEntity = await this.userRepository.findOne({ where: { id } });
    
    console.log("[AUTH_MICROSERVICE] call get from db successfully ", userEntity);
    if (!userEntity)
      throw new NotFoundException(
        `An entity with an ID of ${id} was not found.`,
    );

    return plainToInstance(UserDto, userEntity, {
        excludeExtraneousValues: true
    });
  }
}