
import { UserDto } from '@dexview-application/shared/dtos';
import { UserEntity } from '@dexview-application/shared/entities';
import { Injectable } from '@nestjs/common';
import { InjectLogger, NestjsWinstonLoggerService } from 'nestjs-winston-logger';
import { UserRepository } from './users.repository';

@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository,
    @InjectLogger(UserService.name) private logger: NestjsWinstonLoggerService) {}

  createUser(data: UserDto): void {
    this.userRepository.save(data);
  }

  async getUser(id: number): Promise<UserDto> {
    return await this.userRepository.findOne(id);
  }
}