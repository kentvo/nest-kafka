import { Body, Controller, ParseIntPipe, Post, ValidationPipe } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserDto } from '@dexview-application/shared/dtos';
import { Get } from '@nestjs/common';
import { Param } from '@nestjs/common';
import { InjectLogger, NestjsWinstonLoggerService } from 'nestjs-winston-logger';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    @InjectLogger(AuthController.name) private logger: NestjsWinstonLoggerService
    ) {}

  @Post('sign-up')
  createUser(@Body(ValidationPipe) userDto: UserDto) {
    this.logger.log("Log test post signup");
    return this.authService.createUser(userDto);
  }

  @Get(':id')
  getUser(@Param('id', ParseIntPipe) id: number) {
    return this.authService.getUser(id);
  }
}