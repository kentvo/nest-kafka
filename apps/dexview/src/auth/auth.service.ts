import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { UserDto } from '@dexview-application/shared/dtos';
import { map } from 'rxjs';
import { NestjsWinstonLoggerService, InjectLogger } from "nestjs-winston-logger";

@Injectable()
export class AuthService implements OnModuleInit{
  constructor(
    @Inject('AUTH_MICROSERVICE') 
    private readonly authClient: ClientKafka,
    @InjectLogger(AuthService.name) private logger: NestjsWinstonLoggerService
  ) {}

  onModuleInit() {
    this.authClient.subscribeToResponseOf('get_user');
  }

  createUser(userDto: UserDto) {
    this.logger.log("test xxxx")
    this.authClient.emit('create_user', JSON.stringify(userDto));
  }

  async getUser(id: number)  {
    // console.log("🚀 ~ file: auth.service.ts:17 ~ AuthService ~ getUser ~ id", id)
    await this.authClient.emit('get_user', JSON.stringify(id))
    .pipe(
      map((message: string) => ({ message })),
    );  
  }
}