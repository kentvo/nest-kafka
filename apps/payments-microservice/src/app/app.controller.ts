import { MakePaymentDto } from '@dexview-application/shared/dtos';
import { Controller, Get } from '@nestjs/common';
import { ValidationPipe } from '@nestjs/common/pipes';
import { EventPattern } from '@nestjs/microservices';
import { Payload } from '@nestjs/microservices/decorators';

import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @EventPattern('process_payment')
  handleProcessPayment(@Payload(ValidationPipe) data: MakePaymentDto) {
    this.appService.processPayment(data);
  }
}
