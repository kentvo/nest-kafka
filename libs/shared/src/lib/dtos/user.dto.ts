import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { Expose, Transform } from "class-transformer";

export class UserDto {
  @Expose()
  id: number;

  @IsString()
  @IsNotEmpty()
  @Expose()
  name: string;

  @IsEmail()
  @IsNotEmpty()
  @Expose()
  email: string;

  @Expose()
  isActive: boolean;

  @Expose()
  role: string;
}