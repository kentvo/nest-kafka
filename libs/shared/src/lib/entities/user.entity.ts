import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from "typeorm";

@Entity({name: 'users'})
export class UserEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    email: string

    @Column({
        default: false
    })
    isActive: boolean

    @Column({
        type: 'enum',
        enum: ['admin','user'],
        default: 'admin'
    })
    role: string
}